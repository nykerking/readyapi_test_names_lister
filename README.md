# ReadyAPI Test Names Lister

## Usage
1. Copy `lister.py` and `requirements.txt` to the same directory of ReadyAPI project.
2. Optional but recommended: activate your Python virtualenv
3. Run the following command to install package dependencies.
    ```shell
    pip install -r requirements.txt
    ```
4. Run the following command to export a csv file.
    ```shell
    python lister.py
    ```
5. Remove `lister.py`, `requirements.txt`, and `test_dir.csv` from the current directory upon completion.
