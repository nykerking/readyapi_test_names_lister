import copy
import csv
import os
import traceback
from lxml import etree

# Terminal Colors
BCOLORS_ENDC = "\033[0m"
BCOLORS_FAIL = "\033[91m"
BCOLORS_OKGREEN = "\033[92m"
BCOLORS_WARNING = "\033[93m"

# Initialize CSV filename
CSV_FILE = "test_dir.csv"
# Initialize excluded XML file names
EXCLUDE_XML_FILES = [
    "settings.xml"
]
# Initialize ReadyAPI namespace map
NSMAP = {
    "con": "http://eviware.com/soapui/config"
}
# Initialize XML parser
parser = etree.XMLParser(resolve_entities=False, no_network=True)

# 1. Get all *.xml relative filepath and store it in a list
# https://stackoverflow.com/a/9816863

def absolute_xml_file_paths(directory: str = "."):
    """Yields a generator of absolute XML path strings except files declared in EXCLUDE_XML_FILES.

    Args:
        directory (str, optional): Directory path containing XML files.
        Defaults to ".".

    Yields:
        generator: generator of absolute XML path strings

    Reference:
        https://stackoverflow.com/a/9816863
    """
    for dirpath,_,filenames in os.walk(directory):
        for f in filenames:
            if f.endswith(".xml") and (f not in EXCLUDE_XML_FILES):
                yield os.path.abspath(os.path.join(dirpath, f))

# 2. Parse ReadyAPI XML files
def parse_readyapi_xml_files(xml_filepaths: list):
    """Parses ReadyAPI XML files

    Args:
        xml_filepaths (list): Iterable collection of XML filepath strings

    Returns:
        list: [{
            "filePath": <XML file absolute location>,
            "testCase": <Test case name>,
            "testSuite": <Test suite name>
        }]
    """
    test_dir = []
    for xml_file_path in xml_filepaths:
        try:
            # Creates skeleton
            test_detail = {
                "filePath": xml_file_path,
                "testCase": "",
                "testSuite": ""
            }
            print(
                f"{BCOLORS_WARNING}Parsing"
                f"{BCOLORS_ENDC} {xml_file_path}"
            )
            readyapi_xml = etree.parse(xml_file_path, parser)
            # Get all test case nodes
            test_cases = readyapi_xml.xpath(
                "//con:testCase",
                namespaces=NSMAP
            )
            for test_case in test_cases:
                # Get test case name of the current node
                test_case_name = test_case.xpath(
                    "./@name",
                    namespaces=NSMAP
                )
                # Fill test_detail['testCase']
                test_detail['testCase'] = test_case_name[0]
                # Get test case's test suite node
                test_suite = test_case.xpath(
                    "./ancestor::con:testSuite",
                    namespaces=NSMAP
                )
                # Get test suite name of the current node
                if len(test_suite) > 0:
                    test_suite_name = test_suite[0].xpath(
                        "./@name",
                        namespaces=NSMAP
                    )
                    test_detail['testSuite'] = test_suite_name[0]
                # Add a copy of test_detail dict to test_dir list
                test_dir.append(copy.copy(test_detail))
                # Empty out test_detail['testCase'] and test_detail['testSuite']
                test_detail['testCase'] = ""
                test_detail['testSuite'] = ""
            else:
                print(
                    f"{BCOLORS_OKGREEN}Completed parsing"
                    f"{BCOLORS_ENDC} {xml_file_path}"
                )
        except Exception as e:
            print(
                f"{BCOLORS_FAIL}Encountered error, {e}"
                f"{BCOLORS_ENDC}, in {xml_file_path} and skipped this file."
            )
            print(f"{BCOLORS_FAIL}Traceback:{BCOLORS_ENDC}")
            print(traceback.format_exc())
            continue
    return test_dir

def write_test_dir_csv(test_dir: list, file_name: str = CSV_FILE):
    """Writes `file_name` csv in current directory.

    Args:
        test_dir (list): List of dicts with the following structure:
            [{
                "filePath": <XML file absolute location>,
                "testCase": <Test case name>,
                "testSuite": <Test suite name>
            }]
        file_name (str, optional): The CSV file name. Example: `test_dir.csv`
            Defaults to CSV_FILE.
    """
    # CSV header
    fieldnames = ["filePath", "testCase", "testSuite"]
    # Write CSV
    with open(file_name, "w", encoding="UTF8", newline="") as f:
        writer = csv.DictWriter(f, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(test_dir)

# Main
xml_file_paths = absolute_xml_file_paths(".")
test_dir = parse_readyapi_xml_files(xml_file_paths)
write_test_dir_csv(test_dir)
